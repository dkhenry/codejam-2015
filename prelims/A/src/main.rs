use std::io::BufRead; 
use std::string::String; 

fn readline(input: &mut BufRead, line: &mut String) -> Vec<u64> { 
    input.read_line(line);
    line.trim().split(' ').map(|x| -> u64 { x.parse().unwrap() }).collect()
}

fn main() {
    let mut input = std::io::stdin();
    let mut line = String::new(); 

    input.read_line(&mut line);
    let cases: u64 = line.trim().parse().unwrap();        
    for tc in 0..cases { 
        line.clear(); 
        input.read_line(&mut line);
        let case: Vec<&str> = line.trim().split(' ').collect(); 
        let crowd = case[1]; 
        
        let mut position = 0;
        let mut accum = 0;
        let mut guests = 0; 
        for num in crowd.bytes() { 
            let mut friends = 0; 
            let standers: u8 = num - 48;
            if  position > accum  { 
                friends = position - accum; 
            }
            position = position + 1; 
            accum = accum + standers + friends; 
            guests = guests + friends; 
        }
        // Starting from the left the sum of all the previous numbers
        // must be bigger then the current position. 
        println!("Case #{}: {}",tc+1,guests);
    }
}
