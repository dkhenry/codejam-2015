// Should I split 
// Sum / Max 
// If slip slip all of the highest number then check. 

// The future vrs Now. I can buy i decrement of all plates ( len()*1 )
// right now  or I can buy half of one plate 

//Velocity = splits * decrements 
// Double my speed or move once 


fn eat_for(plates: &Vec<u64>, target: u64) -> u64 {
    let mut count = 0;
    for plate in plates { 
        if *plate > target {
            count = count + (plate / target);
            if plate % target == 0 {
                count = count - 1; 
            }
        }       
    }
    count = count+target; 
    count
}


fn main() {

    let mut input = std::io::stdin();
    let mut line = String::new(); 

    input.read_line(&mut line);
    let cases: u64 = line.trim().parse().unwrap();        
    for tc in 0..cases { 
        line.clear(); 
        input.read_line(&mut line); 
        let diners: u64 = line.trim().parse().unwrap();
        line.clear();
        input.read_line(&mut line);
        let mut plates: Vec<u64> = line.trim().split(' ').map(|x| -> u64 { x.parse().unwrap() }).collect();
        plates.sort();
        plates.reverse();
        let mut min = plates[0];
        let max = plates[0];
        for target in 1..max+1 {            
            let lmin = eat_for(&plates,target);
            min = if lmin < min { lmin } else { min } 
        }
        println!("Case #{}: {}",tc+1,min);
    }
}
