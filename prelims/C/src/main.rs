
enum Quaternions { 
    NegOne,
    ONE,
    NegI,
    I,
    NegJ,
    J,
    NegK,
    K,
 }

fn toQ(a: char) -> Quaternions { 
    match a { 
        'i' => Quaternions::I,
        'j' => Quaternions::J,
        'k' => Quaternions::K
    }
}

fn mul(a: Quaternions, b: Quaternions) -> Quaternions { 
    let p = (a,b);
    match p {
        (Quaternions::NegOne,Quaternions::NegOne) => Quaternions::ONE,
        (Quaternions::NegOne,Quaternions::ONE) => Quaternions::NegOne,
        (Quaternions::NegOne,Quaternions::NegI) => Quaternions::I,
        (Quaternions::NegOne,Quaternions::I) => Quaternions::NegI,
        (Quaternions::NegOne,Quaternions::NegJ) => Quaternions::J,
        (Quaternions::NegOne,Quaternions::J) => Quaternions::NegJ,
        (Quaternions::NegOne,Quaternions::NegK) => Quaternions::K,
        (Quaternions::NegOne,Quaternions::K) => Quaternions::NegK,
 
        (Quaternions::ONE,Quaternions::NegOne) => Quaternions::NegOne,
        (Quaternions::ONE,Quaternions::ONE) => Quaternions::ONE,
        (Quaternions::ONE,Quaternions::NegI) => Quaternions::NegI,
        (Quaternions::ONE,Quaternions::I) => Quaternions::I,
        (Quaternions::ONE,Quaternions::NegJ) => Quaternions::NegJ,
        (Quaternions::ONE,Quaternions::J) => Quaternions::J,
        (Quaternions::ONE,Quaternions::NegK) => Quaternions::NegK,
        (Quaternions::ONE,Quaternions::K) => Quaternions::K,

        (Quaternions::NegI,Quaternions::NegOne) => Quaternions::I,
        (Quaternions::NegI,Quaternions::ONE) => Quaternions::NegI,
        (Quaternions::NegI,Quaternions::NegI) => Quaternions::NegOne,
        (Quaternions::NegI,Quaternions::I) => Quaternions::ONE,
        (Quaternions::NegI,Quaternions::NegJ) => Quaternions::K,
        (Quaternions::NegI,Quaternions::J) => Quaternions::NegK,
        (Quaternions::NegI,Quaternions::NegK) => Quaternions::NegJ,
        (Quaternions::NegI,Quaternions::K) => Quaternions::J,

        (Quaternions::I,Quaternions::NegOne) => Quaternions::NegI,
        (Quaternions::I,Quaternions::ONE) => Quaternions::I,
        (Quaternions::I,Quaternions::NegI) => Quaternions::ONE,
        (Quaternions::I,Quaternions::I) => Quaternions::NegOne,
        (Quaternions::I,Quaternions::NegJ) => Quaternions::NegK,
        (Quaternions::I,Quaternions::J) => Quaternions::K,
        (Quaternions::I,Quaternions::NegK) => Quaternions::J,
        (Quaternions::I,Quaternions::K) => Quaternions::NegJ,

        (Quaternions::NegJ,Quaternions::NegOne) => Quaternions::J,
        (Quaternions::NegJ,Quaternions::ONE) => Quaternions::NegJ,
        (Quaternions::NegJ,Quaternions::NegI) => Quaternions::NegK,
        (Quaternions::NegJ,Quaternions::I) => Quaternions::K,
        (Quaternions::NegJ,Quaternions::NegJ) => Quaternions::ONE,
        (Quaternions::NegJ,Quaternions::J) => Quaternions::NegOne,
        (Quaternions::NegJ,Quaternions::NegK) => Quaternions::I,
        (Quaternions::NegJ,Quaternions::K) => Quaternions::NegI,

        (Quaternions::J,Quaternions::NegOne) => Quaternions::NegJ,
        (Quaternions::J,Quaternions::ONE) => Quaternions::J,
        (Quaternions::J,Quaternions::NegI) => Quaternions::K,
        (Quaternions::J,Quaternions::I) => Quaternions::NegK,
        (Quaternions::J,Quaternions::NegJ) => Quaternions::ONE,
        (Quaternions::J,Quaternions::J) => Quaternions::NegOne,
        (Quaternions::J,Quaternions::NegK) => Quaternions::NegI,
        (Quaternions::J,Quaternions::K) => Quaternions::I,

        (Quaternions::NegK,Quaternions::NegOne) => Quaternions::K,
        (Quaternions::NegK,Quaternions::ONE) => Quaternions::NegK,
        (Quaternions::NegK,Quaternions::NegI) => Quaternions::J,
        (Quaternions::NegK,Quaternions::I) => Quaternions::NegJ,
        (Quaternions::NegK,Quaternions::NegJ) => Quaternions::NegI,
        (Quaternions::NegK,Quaternions::J) => Quaternions::I,
        (Quaternions::NegK,Quaternions::NegK) => Quaternions::NegOne,
        (Quaternions::NegK,Quaternions::K) => Quaternions::ONE,

        (Quaternions::K,Quaternions::NegOne) => Quaternions::NegK,
        (Quaternions::K,Quaternions::ONE) => Quaternions::K,
        (Quaternions::K,Quaternions::NegI) => Quaternions::NegJ,
        (Quaternions::K,Quaternions::I) => Quaternions::J,
        (Quaternions::K,Quaternions::NegJ) => Quaternions::I,
        (Quaternions::K,Quaternions::J) => Quaternions::NegI,
        (Quaternions::K,Quaternions::NegK) => Quaternions::ONE,
        (Quaternions::K,Quaternions::K) => Quaternions::NegOne
    }
}

fn find_i(sequnce: String, index: i) -> Bool { 
    False
}

fn find_j() -> Bool { 
    False
}

fn find_k() -> Bool {
    False
}

fn main() {

    let mut input = std::io::stdin();
    let mut line = String::new(); 

    input.read_line(&mut line);
    let cases: u64 = line.trim().parse().unwrap();        
    for tc in 0..cases { 
        line.clear(); 
        input.read_line(&mut line); 
        let repeat: Vec<u64> = line.trim().split(' ').map(|x| -> u64 { x.parse().unwrap() }).collect();
        line.clear();
        input.read_line(&mut line);
        let mut sequence: String = String::new();
        for _ in 0..repeat[1] { sequence = sequence + line.trim(); }
        
        let index :u64 = 0;
        for i in sequence.chars() { 
            result = 
        }

        println!("Case #{}: {}",tc+1,"NO");
    }
}
