fn main() { 
    let mut input = std::io::stdin();
    let mut line = String::new(); 

    input.read_line(&mut line);
    let cases: u64 = line.trim().parse().unwrap();        
    for tc in 0..cases { 
        line.clear(); 
        input.read_line(&mut line); 
        let params: Vec<u64> = line.trim().split(' ').map(|x| -> u64 { x.parse().unwrap() }).collect();        
        let x = params[0];
        let r = params[1];
        let c = params[2];

        let mut winner="GABRIEL";
        if ( r*c % x != 0 // There is an odd number of blocks ( you can't win )
             || (x > r && x > c) // I can't be bigger in both directions 
             || ( x > 2 && ( x > r+1 || x > c+1 )) // If x is greater then or equal to any of the diameters 
             || x > 7 
           ) { 
            winner="RICHARD";
        }
        println!("Case #{}: {}",tc+1,winner);
    }
}

/*


*/
